﻿namespace CompetitorEF
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.grpComp = new System.Windows.Forms.GroupBox();
            this.list_comp = new System.Windows.Forms.ListView();
            this.clm_comp_name = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clm_comp_age = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clm_comp_nationality = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clm_comp_role = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clm_comp_team = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btn_comp_show = new System.Windows.Forms.Button();
            this.btn_comp_add_display = new System.Windows.Forms.Button();
            this.btn_comp_back = new System.Windows.Forms.Button();
            this.drp_team = new System.Windows.Forms.ComboBox();
            this.organisationBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.btn_comp_add = new System.Windows.Forms.Button();
            this.lbl_comp_team = new System.Windows.Forms.Label();
            this.lbl_comp_role = new System.Windows.Forms.Label();
            this.lbl_comp_nationality = new System.Windows.Forms.Label();
            this.lbl_comp_age = new System.Windows.Forms.Label();
            this.lbl_comp_name = new System.Windows.Forms.Label();
            this.txt_role = new System.Windows.Forms.TextBox();
            this.txt_nationality = new System.Windows.Forms.TextBox();
            this.txt_name = new System.Windows.Forms.TextBox();
            this.grpMenu = new System.Windows.Forms.GroupBox();
            this.btn_menu_skills = new System.Windows.Forms.Button();
            this.btn_menu_coaches = new System.Windows.Forms.Button();
            this.btn_menu_teams = new System.Windows.Forms.Button();
            this.btn_menu_competitors = new System.Windows.Forms.Button();
            this.grpTeam = new System.Windows.Forms.GroupBox();
            this.btn_team_back = new System.Windows.Forms.Button();
            this.list_teams = new System.Windows.Forms.ListView();
            this.clm_Name = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clm_Region = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btn_team_show = new System.Windows.Forms.Button();
            this.btn_team_add_display = new System.Windows.Forms.Button();
            this.btn_team_add_submit = new System.Windows.Forms.Button();
            this.lbl_team_region_display = new System.Windows.Forms.Label();
            this.lbl_team_name_display = new System.Windows.Forms.Label();
            this.txt_team_region = new System.Windows.Forms.TextBox();
            this.txt_team_name = new System.Windows.Forms.TextBox();
            this.grpCoach = new System.Windows.Forms.GroupBox();
            this.drp_coach_team = new System.Windows.Forms.ComboBox();
            this.num_coach_age = new System.Windows.Forms.NumericUpDown();
            this.lbl_coach_team = new System.Windows.Forms.Label();
            this.lbl_coach_nationality = new System.Windows.Forms.Label();
            this.btn_coach_back = new System.Windows.Forms.Button();
            this.list_coach = new System.Windows.Forms.ListView();
            this.clm_coach_name = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clm_coach_ = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clm_coach_nationality = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clm_coach_team = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btn_coach_show = new System.Windows.Forms.Button();
            this.btn_coach_add_display = new System.Windows.Forms.Button();
            this.btn_coach_add_submit = new System.Windows.Forms.Button();
            this.lbl_coach_age = new System.Windows.Forms.Label();
            this.lbl_coach_name = new System.Windows.Forms.Label();
            this.txt_coach_nationality = new System.Windows.Forms.TextBox();
            this.txt_coach_name = new System.Windows.Forms.TextBox();
            this.grpSkill = new System.Windows.Forms.GroupBox();
            this.grp_player_skill = new System.Windows.Forms.GroupBox();
            this.lbl_player_skill_added = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.drp_player_skill_skill = new System.Windows.Forms.ComboBox();
            this.drp_player_skill_player = new System.Windows.Forms.ComboBox();
            this.btn_player_skill_back = new System.Windows.Forms.Button();
            this.btn_player_skill_add = new System.Windows.Forms.Button();
            this.btn_add_comp_skill = new System.Windows.Forms.Button();
            this.lbl_skill_confirm = new System.Windows.Forms.Label();
            this.list_skill_player = new System.Windows.Forms.ListView();
            this.clm_skill_player_name = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lbl_skill_dropSkill = new System.Windows.Forms.Label();
            this.lbl_skill_dropPlayer = new System.Windows.Forms.Label();
            this.drp_skill_skill = new System.Windows.Forms.ComboBox();
            this.drp_skill_player = new System.Windows.Forms.ComboBox();
            this.btn_skill_comp_display = new System.Windows.Forms.Button();
            this.list_skill = new System.Windows.Forms.ListView();
            this.clm_skill_name = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btn_skill_display = new System.Windows.Forms.Button();
            this.btn_skill_add_display = new System.Windows.Forms.Button();
            this.btn_skill_back = new System.Windows.Forms.Button();
            this.btn_skill_add_submit = new System.Windows.Forms.Button();
            this.lbl_skill_name = new System.Windows.Forms.Label();
            this.txt_skill_name = new System.Windows.Forms.TextBox();
            this.lbl_team_added = new System.Windows.Forms.Label();
            this.lbl_comp_added = new System.Windows.Forms.Label();
            this.lbl_coach_added = new System.Windows.Forms.Label();
            this.grpComp.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.organisationBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.grpMenu.SuspendLayout();
            this.grpTeam.SuspendLayout();
            this.grpCoach.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.num_coach_age)).BeginInit();
            this.grpSkill.SuspendLayout();
            this.grp_player_skill.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpComp
            // 
            this.grpComp.Controls.Add(this.lbl_comp_added);
            this.grpComp.Controls.Add(this.list_comp);
            this.grpComp.Controls.Add(this.btn_comp_show);
            this.grpComp.Controls.Add(this.btn_comp_add_display);
            this.grpComp.Controls.Add(this.btn_comp_back);
            this.grpComp.Controls.Add(this.drp_team);
            this.grpComp.Controls.Add(this.numericUpDown1);
            this.grpComp.Controls.Add(this.btn_comp_add);
            this.grpComp.Controls.Add(this.lbl_comp_team);
            this.grpComp.Controls.Add(this.lbl_comp_role);
            this.grpComp.Controls.Add(this.lbl_comp_nationality);
            this.grpComp.Controls.Add(this.lbl_comp_age);
            this.grpComp.Controls.Add(this.lbl_comp_name);
            this.grpComp.Controls.Add(this.txt_role);
            this.grpComp.Controls.Add(this.txt_nationality);
            this.grpComp.Controls.Add(this.txt_name);
            this.grpComp.Location = new System.Drawing.Point(0, 6);
            this.grpComp.Name = "grpComp";
            this.grpComp.Size = new System.Drawing.Size(802, 373);
            this.grpComp.TabIndex = 12;
            this.grpComp.TabStop = false;
            this.grpComp.Text = "Competitors";
            this.grpComp.Visible = false;
            // 
            // list_comp
            // 
            this.list_comp.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.clm_comp_name,
            this.clm_comp_age,
            this.clm_comp_nationality,
            this.clm_comp_role,
            this.clm_comp_team});
            this.list_comp.HideSelection = false;
            this.list_comp.Location = new System.Drawing.Point(12, 114);
            this.list_comp.Name = "list_comp";
            this.list_comp.Size = new System.Drawing.Size(553, 221);
            this.list_comp.TabIndex = 27;
            this.list_comp.UseCompatibleStateImageBehavior = false;
            this.list_comp.View = System.Windows.Forms.View.Details;
            this.list_comp.Visible = false;
            this.list_comp.SelectedIndexChanged += new System.EventHandler(this.list_comp_SelectedIndexChanged);
            // 
            // clm_comp_name
            // 
            this.clm_comp_name.Text = "Name";
            // 
            // clm_comp_age
            // 
            this.clm_comp_age.Text = "Age";
            // 
            // clm_comp_nationality
            // 
            this.clm_comp_nationality.Text = "Nationality";
            // 
            // clm_comp_role
            // 
            this.clm_comp_role.Text = "Role";
            // 
            // clm_comp_team
            // 
            this.clm_comp_team.Text = "Team";
            // 
            // btn_comp_show
            // 
            this.btn_comp_show.Location = new System.Drawing.Point(182, 40);
            this.btn_comp_show.Name = "btn_comp_show";
            this.btn_comp_show.Size = new System.Drawing.Size(157, 50);
            this.btn_comp_show.TabIndex = 26;
            this.btn_comp_show.Text = "Show All";
            this.btn_comp_show.UseVisualStyleBackColor = true;
            this.btn_comp_show.Click += new System.EventHandler(this.btn_comp_show_Click);
            // 
            // btn_comp_add_display
            // 
            this.btn_comp_add_display.Location = new System.Drawing.Point(12, 40);
            this.btn_comp_add_display.Name = "btn_comp_add_display";
            this.btn_comp_add_display.Size = new System.Drawing.Size(157, 50);
            this.btn_comp_add_display.TabIndex = 25;
            this.btn_comp_add_display.Text = "Add";
            this.btn_comp_add_display.UseVisualStyleBackColor = true;
            this.btn_comp_add_display.Click += new System.EventHandler(this.btn_comp_add_display_Click);
            // 
            // btn_comp_back
            // 
            this.btn_comp_back.Location = new System.Drawing.Point(606, 286);
            this.btn_comp_back.Name = "btn_comp_back";
            this.btn_comp_back.Size = new System.Drawing.Size(157, 50);
            this.btn_comp_back.TabIndex = 24;
            this.btn_comp_back.Text = "Back";
            this.btn_comp_back.UseVisualStyleBackColor = true;
            this.btn_comp_back.Click += new System.EventHandler(this.btn_comp_back_Click);
            // 
            // drp_team
            // 
            this.drp_team.DataSource = this.organisationBindingSource;
            this.drp_team.FormattingEnabled = true;
            this.drp_team.Location = new System.Drawing.Point(577, 173);
            this.drp_team.Name = "drp_team";
            this.drp_team.Size = new System.Drawing.Size(219, 28);
            this.drp_team.TabIndex = 23;
            this.drp_team.Visible = false;
            this.drp_team.SelectedIndexChanged += new System.EventHandler(this.drp_team_SelectedIndexChanged);
            // 
            // organisationBindingSource
            // 
            this.organisationBindingSource.DataSource = typeof(Collection.Organisation);
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(577, 72);
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(219, 26);
            this.numericUpDown1.TabIndex = 22;
            this.numericUpDown1.Visible = false;
            this.numericUpDown1.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // btn_comp_add
            // 
            this.btn_comp_add.Location = new System.Drawing.Point(639, 207);
            this.btn_comp_add.Name = "btn_comp_add";
            this.btn_comp_add.Size = new System.Drawing.Size(157, 34);
            this.btn_comp_add.TabIndex = 21;
            this.btn_comp_add.Text = "Add";
            this.btn_comp_add.UseVisualStyleBackColor = true;
            this.btn_comp_add.Visible = false;
            this.btn_comp_add.Click += new System.EventHandler(this.btn_comp_add_Click);
            // 
            // lbl_comp_team
            // 
            this.lbl_comp_team.AutoSize = true;
            this.lbl_comp_team.Location = new System.Drawing.Point(472, 176);
            this.lbl_comp_team.Name = "lbl_comp_team";
            this.lbl_comp_team.Size = new System.Drawing.Size(99, 20);
            this.lbl_comp_team.TabIndex = 20;
            this.lbl_comp_team.Text = "Organisation";
            this.lbl_comp_team.Visible = false;
            // 
            // lbl_comp_role
            // 
            this.lbl_comp_role.AutoSize = true;
            this.lbl_comp_role.Location = new System.Drawing.Point(472, 141);
            this.lbl_comp_role.Name = "lbl_comp_role";
            this.lbl_comp_role.Size = new System.Drawing.Size(42, 20);
            this.lbl_comp_role.TabIndex = 19;
            this.lbl_comp_role.Text = "Role";
            this.lbl_comp_role.Visible = false;
            // 
            // lbl_comp_nationality
            // 
            this.lbl_comp_nationality.AutoSize = true;
            this.lbl_comp_nationality.Location = new System.Drawing.Point(472, 109);
            this.lbl_comp_nationality.Name = "lbl_comp_nationality";
            this.lbl_comp_nationality.Size = new System.Drawing.Size(82, 20);
            this.lbl_comp_nationality.TabIndex = 18;
            this.lbl_comp_nationality.Text = "Nationality";
            this.lbl_comp_nationality.Visible = false;
            // 
            // lbl_comp_age
            // 
            this.lbl_comp_age.AutoSize = true;
            this.lbl_comp_age.Location = new System.Drawing.Point(472, 74);
            this.lbl_comp_age.Name = "lbl_comp_age";
            this.lbl_comp_age.Size = new System.Drawing.Size(38, 20);
            this.lbl_comp_age.TabIndex = 17;
            this.lbl_comp_age.Text = "Age";
            this.lbl_comp_age.Visible = false;
            // 
            // lbl_comp_name
            // 
            this.lbl_comp_name.AutoSize = true;
            this.lbl_comp_name.Location = new System.Drawing.Point(472, 43);
            this.lbl_comp_name.Name = "lbl_comp_name";
            this.lbl_comp_name.Size = new System.Drawing.Size(51, 20);
            this.lbl_comp_name.TabIndex = 16;
            this.lbl_comp_name.Text = "Name";
            this.lbl_comp_name.Visible = false;
            // 
            // txt_role
            // 
            this.txt_role.Location = new System.Drawing.Point(577, 138);
            this.txt_role.Name = "txt_role";
            this.txt_role.Size = new System.Drawing.Size(219, 26);
            this.txt_role.TabIndex = 14;
            this.txt_role.Visible = false;
            this.txt_role.TextChanged += new System.EventHandler(this.txt_role_TextChanged_1);
            // 
            // txt_nationality
            // 
            this.txt_nationality.Location = new System.Drawing.Point(577, 106);
            this.txt_nationality.Name = "txt_nationality";
            this.txt_nationality.Size = new System.Drawing.Size(219, 26);
            this.txt_nationality.TabIndex = 13;
            this.txt_nationality.Visible = false;
            this.txt_nationality.TextChanged += new System.EventHandler(this.txt_nationality_TextChanged_1);
            // 
            // txt_name
            // 
            this.txt_name.Location = new System.Drawing.Point(577, 40);
            this.txt_name.Name = "txt_name";
            this.txt_name.Size = new System.Drawing.Size(219, 26);
            this.txt_name.TabIndex = 12;
            this.txt_name.Visible = false;
            this.txt_name.TextChanged += new System.EventHandler(this.txt_name_TextChanged);
            // 
            // grpMenu
            // 
            this.grpMenu.Controls.Add(this.btn_menu_skills);
            this.grpMenu.Controls.Add(this.btn_menu_coaches);
            this.grpMenu.Controls.Add(this.btn_menu_teams);
            this.grpMenu.Controls.Add(this.btn_menu_competitors);
            this.grpMenu.Location = new System.Drawing.Point(6, 6);
            this.grpMenu.Name = "grpMenu";
            this.grpMenu.Size = new System.Drawing.Size(796, 373);
            this.grpMenu.TabIndex = 23;
            this.grpMenu.TabStop = false;
            this.grpMenu.Text = "Menu";
            // 
            // btn_menu_skills
            // 
            this.btn_menu_skills.Location = new System.Drawing.Point(605, 34);
            this.btn_menu_skills.Name = "btn_menu_skills";
            this.btn_menu_skills.Size = new System.Drawing.Size(164, 68);
            this.btn_menu_skills.TabIndex = 3;
            this.btn_menu_skills.Text = "Champions";
            this.btn_menu_skills.UseVisualStyleBackColor = true;
            this.btn_menu_skills.Click += new System.EventHandler(this.btn_menu_skills_Click);
            // 
            // btn_menu_coaches
            // 
            this.btn_menu_coaches.Location = new System.Drawing.Point(417, 34);
            this.btn_menu_coaches.Name = "btn_menu_coaches";
            this.btn_menu_coaches.Size = new System.Drawing.Size(164, 68);
            this.btn_menu_coaches.TabIndex = 2;
            this.btn_menu_coaches.Text = "Coaches";
            this.btn_menu_coaches.UseVisualStyleBackColor = true;
            this.btn_menu_coaches.Click += new System.EventHandler(this.btn_menu_coaches_Click);
            // 
            // btn_menu_teams
            // 
            this.btn_menu_teams.Location = new System.Drawing.Point(224, 34);
            this.btn_menu_teams.Name = "btn_menu_teams";
            this.btn_menu_teams.Size = new System.Drawing.Size(164, 68);
            this.btn_menu_teams.TabIndex = 1;
            this.btn_menu_teams.Text = "Teams";
            this.btn_menu_teams.UseVisualStyleBackColor = true;
            this.btn_menu_teams.Click += new System.EventHandler(this.btn_menu_teams_Click);
            // 
            // btn_menu_competitors
            // 
            this.btn_menu_competitors.Location = new System.Drawing.Point(32, 34);
            this.btn_menu_competitors.Name = "btn_menu_competitors";
            this.btn_menu_competitors.Size = new System.Drawing.Size(164, 68);
            this.btn_menu_competitors.TabIndex = 0;
            this.btn_menu_competitors.Text = "Competitors";
            this.btn_menu_competitors.UseVisualStyleBackColor = true;
            this.btn_menu_competitors.Click += new System.EventHandler(this.btn_menu_competitors_Click);
            // 
            // grpTeam
            // 
            this.grpTeam.Controls.Add(this.lbl_team_added);
            this.grpTeam.Controls.Add(this.btn_team_back);
            this.grpTeam.Controls.Add(this.list_teams);
            this.grpTeam.Controls.Add(this.btn_team_show);
            this.grpTeam.Controls.Add(this.btn_team_add_display);
            this.grpTeam.Controls.Add(this.btn_team_add_submit);
            this.grpTeam.Controls.Add(this.lbl_team_region_display);
            this.grpTeam.Controls.Add(this.lbl_team_name_display);
            this.grpTeam.Controls.Add(this.txt_team_region);
            this.grpTeam.Controls.Add(this.txt_team_name);
            this.grpTeam.Location = new System.Drawing.Point(12, 8);
            this.grpTeam.Name = "grpTeam";
            this.grpTeam.Size = new System.Drawing.Size(796, 371);
            this.grpTeam.TabIndex = 4;
            this.grpTeam.TabStop = false;
            this.grpTeam.Text = "Teams";
            this.grpTeam.Visible = false;
            // 
            // btn_team_back
            // 
            this.btn_team_back.Location = new System.Drawing.Point(582, 302);
            this.btn_team_back.Name = "btn_team_back";
            this.btn_team_back.Size = new System.Drawing.Size(200, 50);
            this.btn_team_back.TabIndex = 8;
            this.btn_team_back.Text = "Back";
            this.btn_team_back.UseVisualStyleBackColor = true;
            this.btn_team_back.Click += new System.EventHandler(this.btn_team_back_Click);
            // 
            // list_teams
            // 
            this.list_teams.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.clm_Name,
            this.clm_Region});
            this.list_teams.HideSelection = false;
            this.list_teams.Location = new System.Drawing.Point(17, 118);
            this.list_teams.Name = "list_teams";
            this.list_teams.Size = new System.Drawing.Size(310, 234);
            this.list_teams.TabIndex = 7;
            this.list_teams.UseCompatibleStateImageBehavior = false;
            this.list_teams.View = System.Windows.Forms.View.Details;
            this.list_teams.Visible = false;
            // 
            // clm_Name
            // 
            this.clm_Name.Text = "Name";
            this.clm_Name.Width = 140;
            // 
            // clm_Region
            // 
            this.clm_Region.Text = "Region";
            this.clm_Region.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.clm_Region.Width = 50;
            // 
            // btn_team_show
            // 
            this.btn_team_show.Location = new System.Drawing.Point(184, 44);
            this.btn_team_show.Name = "btn_team_show";
            this.btn_team_show.Size = new System.Drawing.Size(143, 45);
            this.btn_team_show.TabIndex = 6;
            this.btn_team_show.Text = "Show Teams";
            this.btn_team_show.UseVisualStyleBackColor = true;
            this.btn_team_show.Click += new System.EventHandler(this.btn_team_show_Click);
            // 
            // btn_team_add_display
            // 
            this.btn_team_add_display.Location = new System.Drawing.Point(17, 45);
            this.btn_team_add_display.Name = "btn_team_add_display";
            this.btn_team_add_display.Size = new System.Drawing.Size(143, 45);
            this.btn_team_add_display.TabIndex = 5;
            this.btn_team_add_display.Text = "Add";
            this.btn_team_add_display.UseVisualStyleBackColor = true;
            this.btn_team_add_display.Click += new System.EventHandler(this.btn_team_add_display_Click);
            // 
            // btn_team_add_submit
            // 
            this.btn_team_add_submit.Location = new System.Drawing.Point(597, 217);
            this.btn_team_add_submit.Name = "btn_team_add_submit";
            this.btn_team_add_submit.Size = new System.Drawing.Size(184, 30);
            this.btn_team_add_submit.TabIndex = 4;
            this.btn_team_add_submit.Text = "Add Team";
            this.btn_team_add_submit.UseVisualStyleBackColor = true;
            this.btn_team_add_submit.Visible = false;
            this.btn_team_add_submit.Click += new System.EventHandler(this.btn_team_add_submit_Click);
            // 
            // lbl_team_region_display
            // 
            this.lbl_team_region_display.AutoSize = true;
            this.lbl_team_region_display.Location = new System.Drawing.Point(404, 171);
            this.lbl_team_region_display.Name = "lbl_team_region_display";
            this.lbl_team_region_display.Size = new System.Drawing.Size(60, 20);
            this.lbl_team_region_display.TabIndex = 3;
            this.lbl_team_region_display.Text = "Region";
            this.lbl_team_region_display.Visible = false;
            // 
            // lbl_team_name_display
            // 
            this.lbl_team_name_display.AutoSize = true;
            this.lbl_team_name_display.Location = new System.Drawing.Point(404, 139);
            this.lbl_team_name_display.Name = "lbl_team_name_display";
            this.lbl_team_name_display.Size = new System.Drawing.Size(55, 20);
            this.lbl_team_name_display.TabIndex = 2;
            this.lbl_team_name_display.Text = "Name:";
            this.lbl_team_name_display.Visible = false;
            // 
            // txt_team_region
            // 
            this.txt_team_region.Location = new System.Drawing.Point(479, 168);
            this.txt_team_region.Name = "txt_team_region";
            this.txt_team_region.Size = new System.Drawing.Size(303, 26);
            this.txt_team_region.TabIndex = 1;
            this.txt_team_region.Visible = false;
            this.txt_team_region.TextChanged += new System.EventHandler(this.txt_team_region_TextChanged);
            // 
            // txt_team_name
            // 
            this.txt_team_name.Location = new System.Drawing.Point(479, 136);
            this.txt_team_name.Name = "txt_team_name";
            this.txt_team_name.Size = new System.Drawing.Size(303, 26);
            this.txt_team_name.TabIndex = 0;
            this.txt_team_name.Visible = false;
            this.txt_team_name.TextChanged += new System.EventHandler(this.txt_team_name_TextChanged);
            // 
            // grpCoach
            // 
            this.grpCoach.Controls.Add(this.lbl_coach_added);
            this.grpCoach.Controls.Add(this.drp_coach_team);
            this.grpCoach.Controls.Add(this.num_coach_age);
            this.grpCoach.Controls.Add(this.lbl_coach_team);
            this.grpCoach.Controls.Add(this.lbl_coach_nationality);
            this.grpCoach.Controls.Add(this.btn_coach_back);
            this.grpCoach.Controls.Add(this.list_coach);
            this.grpCoach.Controls.Add(this.btn_coach_show);
            this.grpCoach.Controls.Add(this.btn_coach_add_display);
            this.grpCoach.Controls.Add(this.btn_coach_add_submit);
            this.grpCoach.Controls.Add(this.lbl_coach_age);
            this.grpCoach.Controls.Add(this.lbl_coach_name);
            this.grpCoach.Controls.Add(this.txt_coach_nationality);
            this.grpCoach.Controls.Add(this.txt_coach_name);
            this.grpCoach.Location = new System.Drawing.Point(6, 6);
            this.grpCoach.Name = "grpCoach";
            this.grpCoach.Size = new System.Drawing.Size(796, 373);
            this.grpCoach.TabIndex = 24;
            this.grpCoach.TabStop = false;
            this.grpCoach.Text = "Coaches";
            this.grpCoach.Visible = false;
            // 
            // drp_coach_team
            // 
            this.drp_coach_team.DataSource = this.organisationBindingSource;
            this.drp_coach_team.FormattingEnabled = true;
            this.drp_coach_team.Location = new System.Drawing.Point(510, 150);
            this.drp_coach_team.Name = "drp_coach_team";
            this.drp_coach_team.Size = new System.Drawing.Size(276, 28);
            this.drp_coach_team.TabIndex = 25;
            this.drp_coach_team.Visible = false;
            this.drp_coach_team.SelectedIndexChanged += new System.EventHandler(this.drp_coach_team_SelectedIndexChanged);
            // 
            // num_coach_age
            // 
            this.num_coach_age.Location = new System.Drawing.Point(510, 89);
            this.num_coach_age.Name = "num_coach_age";
            this.num_coach_age.Size = new System.Drawing.Size(276, 26);
            this.num_coach_age.TabIndex = 24;
            this.num_coach_age.Visible = false;
            this.num_coach_age.ValueChanged += new System.EventHandler(this.num_coach_age_ValueChanged);
            // 
            // lbl_coach_team
            // 
            this.lbl_coach_team.AutoSize = true;
            this.lbl_coach_team.Location = new System.Drawing.Point(412, 153);
            this.lbl_coach_team.Name = "lbl_coach_team";
            this.lbl_coach_team.Size = new System.Drawing.Size(53, 20);
            this.lbl_coach_team.TabIndex = 19;
            this.lbl_coach_team.Text = "Team:";
            this.lbl_coach_team.Visible = false;
            // 
            // lbl_coach_nationality
            // 
            this.lbl_coach_nationality.AutoSize = true;
            this.lbl_coach_nationality.Location = new System.Drawing.Point(412, 121);
            this.lbl_coach_nationality.Name = "lbl_coach_nationality";
            this.lbl_coach_nationality.Size = new System.Drawing.Size(86, 20);
            this.lbl_coach_nationality.TabIndex = 18;
            this.lbl_coach_nationality.Text = "Nationality:";
            this.lbl_coach_nationality.Visible = false;
            // 
            // btn_coach_back
            // 
            this.btn_coach_back.Location = new System.Drawing.Point(581, 290);
            this.btn_coach_back.Name = "btn_coach_back";
            this.btn_coach_back.Size = new System.Drawing.Size(200, 50);
            this.btn_coach_back.TabIndex = 17;
            this.btn_coach_back.Text = "Back";
            this.btn_coach_back.UseVisualStyleBackColor = true;
            this.btn_coach_back.Click += new System.EventHandler(this.btn_coach_back_Click);
            // 
            // list_coach
            // 
            this.list_coach.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.clm_coach_name,
            this.clm_coach_,
            this.clm_coach_nationality,
            this.clm_coach_team});
            this.list_coach.HideSelection = false;
            this.list_coach.Location = new System.Drawing.Point(16, 106);
            this.list_coach.Name = "list_coach";
            this.list_coach.Size = new System.Drawing.Size(559, 234);
            this.list_coach.TabIndex = 16;
            this.list_coach.UseCompatibleStateImageBehavior = false;
            this.list_coach.View = System.Windows.Forms.View.Details;
            this.list_coach.Visible = false;
            // 
            // clm_coach_name
            // 
            this.clm_coach_name.Text = "Name";
            this.clm_coach_name.Width = 140;
            // 
            // clm_coach_
            // 
            this.clm_coach_.Text = "Age";
            this.clm_coach_.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.clm_coach_.Width = 50;
            // 
            // clm_coach_nationality
            // 
            this.clm_coach_nationality.Text = "Nationality";
            // 
            // clm_coach_team
            // 
            this.clm_coach_team.Text = "Team";
            // 
            // btn_coach_show
            // 
            this.btn_coach_show.Location = new System.Drawing.Point(183, 32);
            this.btn_coach_show.Name = "btn_coach_show";
            this.btn_coach_show.Size = new System.Drawing.Size(143, 45);
            this.btn_coach_show.TabIndex = 15;
            this.btn_coach_show.Text = "Show Coaches";
            this.btn_coach_show.UseVisualStyleBackColor = true;
            this.btn_coach_show.Click += new System.EventHandler(this.btn_coach_show_Click);
            // 
            // btn_coach_add_display
            // 
            this.btn_coach_add_display.Location = new System.Drawing.Point(16, 33);
            this.btn_coach_add_display.Name = "btn_coach_add_display";
            this.btn_coach_add_display.Size = new System.Drawing.Size(143, 45);
            this.btn_coach_add_display.TabIndex = 14;
            this.btn_coach_add_display.Text = "Add";
            this.btn_coach_add_display.UseVisualStyleBackColor = true;
            this.btn_coach_add_display.Click += new System.EventHandler(this.btn_coach_add_display_Click);
            // 
            // btn_coach_add_submit
            // 
            this.btn_coach_add_submit.Location = new System.Drawing.Point(596, 205);
            this.btn_coach_add_submit.Name = "btn_coach_add_submit";
            this.btn_coach_add_submit.Size = new System.Drawing.Size(184, 30);
            this.btn_coach_add_submit.TabIndex = 13;
            this.btn_coach_add_submit.Text = "Add Coach";
            this.btn_coach_add_submit.UseVisualStyleBackColor = true;
            this.btn_coach_add_submit.Visible = false;
            this.btn_coach_add_submit.Click += new System.EventHandler(this.btn_coach_add_submit_Click);
            // 
            // lbl_coach_age
            // 
            this.lbl_coach_age.AutoSize = true;
            this.lbl_coach_age.Location = new System.Drawing.Point(412, 91);
            this.lbl_coach_age.Name = "lbl_coach_age";
            this.lbl_coach_age.Size = new System.Drawing.Size(42, 20);
            this.lbl_coach_age.TabIndex = 12;
            this.lbl_coach_age.Text = "Age:";
            this.lbl_coach_age.Visible = false;
            // 
            // lbl_coach_name
            // 
            this.lbl_coach_name.AutoSize = true;
            this.lbl_coach_name.Location = new System.Drawing.Point(412, 69);
            this.lbl_coach_name.Name = "lbl_coach_name";
            this.lbl_coach_name.Size = new System.Drawing.Size(55, 20);
            this.lbl_coach_name.TabIndex = 11;
            this.lbl_coach_name.Text = "Name:";
            this.lbl_coach_name.Visible = false;
            // 
            // txt_coach_nationality
            // 
            this.txt_coach_nationality.Location = new System.Drawing.Point(510, 118);
            this.txt_coach_nationality.Name = "txt_coach_nationality";
            this.txt_coach_nationality.Size = new System.Drawing.Size(276, 26);
            this.txt_coach_nationality.TabIndex = 10;
            this.txt_coach_nationality.Visible = false;
            this.txt_coach_nationality.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // txt_coach_name
            // 
            this.txt_coach_name.Location = new System.Drawing.Point(510, 61);
            this.txt_coach_name.Name = "txt_coach_name";
            this.txt_coach_name.Size = new System.Drawing.Size(276, 26);
            this.txt_coach_name.TabIndex = 9;
            this.txt_coach_name.Visible = false;
            this.txt_coach_name.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // grpSkill
            // 
            this.grpSkill.Controls.Add(this.btn_add_comp_skill);
            this.grpSkill.Controls.Add(this.lbl_skill_confirm);
            this.grpSkill.Controls.Add(this.list_skill_player);
            this.grpSkill.Controls.Add(this.lbl_skill_dropSkill);
            this.grpSkill.Controls.Add(this.lbl_skill_dropPlayer);
            this.grpSkill.Controls.Add(this.drp_skill_skill);
            this.grpSkill.Controls.Add(this.drp_skill_player);
            this.grpSkill.Controls.Add(this.btn_skill_comp_display);
            this.grpSkill.Controls.Add(this.list_skill);
            this.grpSkill.Controls.Add(this.btn_skill_display);
            this.grpSkill.Controls.Add(this.btn_skill_add_display);
            this.grpSkill.Controls.Add(this.btn_skill_back);
            this.grpSkill.Controls.Add(this.btn_skill_add_submit);
            this.grpSkill.Controls.Add(this.lbl_skill_name);
            this.grpSkill.Controls.Add(this.txt_skill_name);
            this.grpSkill.Controls.Add(this.grp_player_skill);
            this.grpSkill.Location = new System.Drawing.Point(0, 0);
            this.grpSkill.Name = "grpSkill";
            this.grpSkill.Size = new System.Drawing.Size(796, 373);
            this.grpSkill.TabIndex = 24;
            this.grpSkill.TabStop = false;
            this.grpSkill.Text = "Champions";
            this.grpSkill.Visible = false;
            // 
            // grp_player_skill
            // 
            this.grp_player_skill.Controls.Add(this.lbl_player_skill_added);
            this.grp_player_skill.Controls.Add(this.label2);
            this.grp_player_skill.Controls.Add(this.label3);
            this.grp_player_skill.Controls.Add(this.drp_player_skill_skill);
            this.grp_player_skill.Controls.Add(this.drp_player_skill_player);
            this.grp_player_skill.Controls.Add(this.btn_player_skill_back);
            this.grp_player_skill.Controls.Add(this.btn_player_skill_add);
            this.grp_player_skill.Location = new System.Drawing.Point(0, 0);
            this.grp_player_skill.Name = "grp_player_skill";
            this.grp_player_skill.Size = new System.Drawing.Size(796, 373);
            this.grp_player_skill.TabIndex = 51;
            this.grp_player_skill.TabStop = false;
            this.grp_player_skill.Text = "Add Player Champs";
            this.grp_player_skill.Visible = false;
            // 
            // lbl_player_skill_added
            // 
            this.lbl_player_skill_added.AutoSize = true;
            this.lbl_player_skill_added.Location = new System.Drawing.Point(476, 187);
            this.lbl_player_skill_added.Name = "lbl_player_skill_added";
            this.lbl_player_skill_added.Size = new System.Drawing.Size(0, 20);
            this.lbl_player_skill_added.TabIndex = 49;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(192, 187);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 20);
            this.label2.TabIndex = 47;
            this.label2.Text = "Champ:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(192, 120);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 20);
            this.label3.TabIndex = 46;
            this.label3.Text = "Player:";
            // 
            // drp_player_skill_skill
            // 
            this.drp_player_skill_skill.FormattingEnabled = true;
            this.drp_player_skill_skill.Location = new System.Drawing.Point(196, 217);
            this.drp_player_skill_skill.Name = "drp_player_skill_skill";
            this.drp_player_skill_skill.Size = new System.Drawing.Size(237, 28);
            this.drp_player_skill_skill.TabIndex = 45;
            this.drp_player_skill_skill.SelectedIndexChanged += new System.EventHandler(this.drp_player_skill_skill_SelectedIndexChanged);
            // 
            // drp_player_skill_player
            // 
            this.drp_player_skill_player.FormattingEnabled = true;
            this.drp_player_skill_player.Location = new System.Drawing.Point(196, 151);
            this.drp_player_skill_player.Name = "drp_player_skill_player";
            this.drp_player_skill_player.Size = new System.Drawing.Size(237, 28);
            this.drp_player_skill_player.TabIndex = 44;
            this.drp_player_skill_player.SelectedIndexChanged += new System.EventHandler(this.drp_player_skill_player_SelectedIndexChanged);
            // 
            // btn_player_skill_back
            // 
            this.btn_player_skill_back.Location = new System.Drawing.Point(600, 284);
            this.btn_player_skill_back.Name = "btn_player_skill_back";
            this.btn_player_skill_back.Size = new System.Drawing.Size(157, 50);
            this.btn_player_skill_back.TabIndex = 39;
            this.btn_player_skill_back.Text = "Back";
            this.btn_player_skill_back.UseVisualStyleBackColor = true;
            this.btn_player_skill_back.Click += new System.EventHandler(this.btn_player_skill_back_Click);
            // 
            // btn_player_skill_add
            // 
            this.btn_player_skill_add.Location = new System.Drawing.Point(276, 251);
            this.btn_player_skill_add.Name = "btn_player_skill_add";
            this.btn_player_skill_add.Size = new System.Drawing.Size(157, 34);
            this.btn_player_skill_add.TabIndex = 36;
            this.btn_player_skill_add.Text = "Add";
            this.btn_player_skill_add.UseVisualStyleBackColor = true;
            this.btn_player_skill_add.Click += new System.EventHandler(this.btn_player_skill_add_Click);
            // 
            // btn_add_comp_skill
            // 
            this.btn_add_comp_skill.Location = new System.Drawing.Point(196, 259);
            this.btn_add_comp_skill.Name = "btn_add_comp_skill";
            this.btn_add_comp_skill.Size = new System.Drawing.Size(237, 74);
            this.btn_add_comp_skill.TabIndex = 50;
            this.btn_add_comp_skill.Text = "Add Player Champs";
            this.btn_add_comp_skill.UseVisualStyleBackColor = true;
            this.btn_add_comp_skill.Visible = false;
            this.btn_add_comp_skill.Click += new System.EventHandler(this.btn_add_comp_skill_Click);
            // 
            // lbl_skill_confirm
            // 
            this.lbl_skill_confirm.AutoSize = true;
            this.lbl_skill_confirm.Location = new System.Drawing.Point(365, 307);
            this.lbl_skill_confirm.Name = "lbl_skill_confirm";
            this.lbl_skill_confirm.Size = new System.Drawing.Size(0, 20);
            this.lbl_skill_confirm.TabIndex = 49;
            this.lbl_skill_confirm.Visible = false;
            // 
            // list_skill_player
            // 
            this.list_skill_player.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.clm_skill_player_name});
            this.list_skill_player.HideSelection = false;
            this.list_skill_player.Location = new System.Drawing.Point(527, 16);
            this.list_skill_player.Name = "list_skill_player";
            this.list_skill_player.Size = new System.Drawing.Size(263, 229);
            this.list_skill_player.TabIndex = 48;
            this.list_skill_player.UseCompatibleStateImageBehavior = false;
            this.list_skill_player.View = System.Windows.Forms.View.List;
            this.list_skill_player.Visible = false;
            // 
            // clm_skill_player_name
            // 
            this.clm_skill_player_name.Text = "Name";
            // 
            // lbl_skill_dropSkill
            // 
            this.lbl_skill_dropSkill.AutoSize = true;
            this.lbl_skill_dropSkill.Location = new System.Drawing.Point(192, 187);
            this.lbl_skill_dropSkill.Name = "lbl_skill_dropSkill";
            this.lbl_skill_dropSkill.Size = new System.Drawing.Size(64, 20);
            this.lbl_skill_dropSkill.TabIndex = 47;
            this.lbl_skill_dropSkill.Text = "Champ:";
            this.lbl_skill_dropSkill.Visible = false;
            // 
            // lbl_skill_dropPlayer
            // 
            this.lbl_skill_dropPlayer.AutoSize = true;
            this.lbl_skill_dropPlayer.Location = new System.Drawing.Point(192, 120);
            this.lbl_skill_dropPlayer.Name = "lbl_skill_dropPlayer";
            this.lbl_skill_dropPlayer.Size = new System.Drawing.Size(56, 20);
            this.lbl_skill_dropPlayer.TabIndex = 46;
            this.lbl_skill_dropPlayer.Text = "Player:";
            this.lbl_skill_dropPlayer.Visible = false;
            // 
            // drp_skill_skill
            // 
            this.drp_skill_skill.FormattingEnabled = true;
            this.drp_skill_skill.Location = new System.Drawing.Point(196, 217);
            this.drp_skill_skill.Name = "drp_skill_skill";
            this.drp_skill_skill.Size = new System.Drawing.Size(237, 28);
            this.drp_skill_skill.TabIndex = 45;
            this.drp_skill_skill.Visible = false;
            this.drp_skill_skill.SelectedIndexChanged += new System.EventHandler(this.drp_skill_skill_SelectedIndexChanged);
            // 
            // drp_skill_player
            // 
            this.drp_skill_player.FormattingEnabled = true;
            this.drp_skill_player.Location = new System.Drawing.Point(196, 151);
            this.drp_skill_player.Name = "drp_skill_player";
            this.drp_skill_player.Size = new System.Drawing.Size(237, 28);
            this.drp_skill_player.TabIndex = 44;
            this.drp_skill_player.Visible = false;
            this.drp_skill_player.SelectedIndexChanged += new System.EventHandler(this.drp_skill_player_SelectedIndexChanged);
            // 
            // btn_skill_comp_display
            // 
            this.btn_skill_comp_display.Location = new System.Drawing.Point(347, 38);
            this.btn_skill_comp_display.Name = "btn_skill_comp_display";
            this.btn_skill_comp_display.Size = new System.Drawing.Size(157, 50);
            this.btn_skill_comp_display.TabIndex = 43;
            this.btn_skill_comp_display.Text = "Player Champs";
            this.btn_skill_comp_display.UseVisualStyleBackColor = true;
            this.btn_skill_comp_display.Click += new System.EventHandler(this.btn_skill_comp_display_Click);
            // 
            // list_skill
            // 
            this.list_skill.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.clm_skill_name});
            this.list_skill.HideSelection = false;
            this.list_skill.Location = new System.Drawing.Point(6, 112);
            this.list_skill.Name = "list_skill";
            this.list_skill.Size = new System.Drawing.Size(157, 221);
            this.list_skill.TabIndex = 42;
            this.list_skill.UseCompatibleStateImageBehavior = false;
            this.list_skill.View = System.Windows.Forms.View.Details;
            this.list_skill.Visible = false;
            // 
            // clm_skill_name
            // 
            this.clm_skill_name.Text = "Name";
            // 
            // btn_skill_display
            // 
            this.btn_skill_display.Location = new System.Drawing.Point(176, 38);
            this.btn_skill_display.Name = "btn_skill_display";
            this.btn_skill_display.Size = new System.Drawing.Size(157, 50);
            this.btn_skill_display.TabIndex = 41;
            this.btn_skill_display.Text = "Show All";
            this.btn_skill_display.UseVisualStyleBackColor = true;
            this.btn_skill_display.Click += new System.EventHandler(this.btn_skill_display_Click);
            // 
            // btn_skill_add_display
            // 
            this.btn_skill_add_display.Location = new System.Drawing.Point(6, 38);
            this.btn_skill_add_display.Name = "btn_skill_add_display";
            this.btn_skill_add_display.Size = new System.Drawing.Size(157, 50);
            this.btn_skill_add_display.TabIndex = 40;
            this.btn_skill_add_display.Text = "Add";
            this.btn_skill_add_display.UseVisualStyleBackColor = true;
            this.btn_skill_add_display.Click += new System.EventHandler(this.btn_skill_add_display_Click);
            // 
            // btn_skill_back
            // 
            this.btn_skill_back.Location = new System.Drawing.Point(600, 284);
            this.btn_skill_back.Name = "btn_skill_back";
            this.btn_skill_back.Size = new System.Drawing.Size(157, 50);
            this.btn_skill_back.TabIndex = 39;
            this.btn_skill_back.Text = "Back";
            this.btn_skill_back.UseVisualStyleBackColor = true;
            this.btn_skill_back.Click += new System.EventHandler(this.btn_skill_back_Click);
            // 
            // btn_skill_add_submit
            // 
            this.btn_skill_add_submit.Location = new System.Drawing.Point(633, 205);
            this.btn_skill_add_submit.Name = "btn_skill_add_submit";
            this.btn_skill_add_submit.Size = new System.Drawing.Size(157, 34);
            this.btn_skill_add_submit.TabIndex = 36;
            this.btn_skill_add_submit.Text = "Add";
            this.btn_skill_add_submit.UseVisualStyleBackColor = true;
            this.btn_skill_add_submit.Visible = false;
            this.btn_skill_add_submit.Click += new System.EventHandler(this.btn_skill_add_submit_Click);
            // 
            // lbl_skill_name
            // 
            this.lbl_skill_name.AutoSize = true;
            this.lbl_skill_name.Location = new System.Drawing.Point(503, 147);
            this.lbl_skill_name.Name = "lbl_skill_name";
            this.lbl_skill_name.Size = new System.Drawing.Size(55, 20);
            this.lbl_skill_name.TabIndex = 31;
            this.lbl_skill_name.Text = "Name:";
            this.lbl_skill_name.Visible = false;
            // 
            // txt_skill_name
            // 
            this.txt_skill_name.Location = new System.Drawing.Point(573, 147);
            this.txt_skill_name.Name = "txt_skill_name";
            this.txt_skill_name.Size = new System.Drawing.Size(219, 26);
            this.txt_skill_name.TabIndex = 28;
            this.txt_skill_name.Visible = false;
            this.txt_skill_name.TextChanged += new System.EventHandler(this.txt_skill_name_TextChanged);
            // 
            // lbl_team_added
            // 
            this.lbl_team_added.AutoSize = true;
            this.lbl_team_added.Location = new System.Drawing.Point(327, 257);
            this.lbl_team_added.Name = "lbl_team_added";
            this.lbl_team_added.Size = new System.Drawing.Size(0, 20);
            this.lbl_team_added.TabIndex = 9;
            this.lbl_team_added.Visible = false;
            // 
            // lbl_comp_added
            // 
            this.lbl_comp_added.AutoSize = true;
            this.lbl_comp_added.Location = new System.Drawing.Point(268, 259);
            this.lbl_comp_added.Name = "lbl_comp_added";
            this.lbl_comp_added.Size = new System.Drawing.Size(0, 23);
            this.lbl_comp_added.TabIndex = 28;
            this.lbl_comp_added.UseCompatibleTextRendering = true;
            this.lbl_comp_added.Visible = false;
            // 
            // lbl_coach_added
            // 
            this.lbl_coach_added.AutoSize = true;
            this.lbl_coach_added.Location = new System.Drawing.Point(258, 259);
            this.lbl_coach_added.Name = "lbl_coach_added";
            this.lbl_coach_added.Size = new System.Drawing.Size(0, 20);
            this.lbl_coach_added.TabIndex = 26;
            this.lbl_coach_added.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.grpSkill);
            this.Controls.Add(this.grpCoach);
            this.Controls.Add(this.grpMenu);
            this.Controls.Add(this.grpTeam);
            this.Controls.Add(this.grpComp);
            this.Name = "Form1";
            this.Text = "League of Legends";
            this.grpComp.ResumeLayout(false);
            this.grpComp.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.organisationBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.grpMenu.ResumeLayout(false);
            this.grpTeam.ResumeLayout(false);
            this.grpTeam.PerformLayout();
            this.grpCoach.ResumeLayout(false);
            this.grpCoach.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.num_coach_age)).EndInit();
            this.grpSkill.ResumeLayout(false);
            this.grpSkill.PerformLayout();
            this.grp_player_skill.ResumeLayout(false);
            this.grp_player_skill.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox grpComp;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Button btn_comp_add;
        private System.Windows.Forms.Label lbl_comp_team;
        private System.Windows.Forms.Label lbl_comp_role;
        private System.Windows.Forms.Label lbl_comp_nationality;
        private System.Windows.Forms.Label lbl_comp_age;
        private System.Windows.Forms.Label lbl_comp_name;
        private System.Windows.Forms.TextBox txt_role;
        private System.Windows.Forms.TextBox txt_nationality;
        private System.Windows.Forms.TextBox txt_name;
        private System.Windows.Forms.GroupBox grpMenu;
        private System.Windows.Forms.GroupBox grpTeam;
        private System.Windows.Forms.Button btn_team_show;
        private System.Windows.Forms.Button btn_team_add_display;
        private System.Windows.Forms.Button btn_team_add_submit;
        private System.Windows.Forms.Label lbl_team_region_display;
        private System.Windows.Forms.Label lbl_team_name_display;
        private System.Windows.Forms.TextBox txt_team_region;
        private System.Windows.Forms.TextBox txt_team_name;
        private System.Windows.Forms.Button btn_menu_skills;
        private System.Windows.Forms.Button btn_menu_coaches;
        private System.Windows.Forms.Button btn_menu_teams;
        private System.Windows.Forms.Button btn_menu_competitors;
        private System.Windows.Forms.ListView list_teams;
        private System.Windows.Forms.BindingSource organisationBindingSource;
        private System.Windows.Forms.ColumnHeader clm_Name;
        private System.Windows.Forms.ColumnHeader clm_Region;
        private System.Windows.Forms.Button btn_team_back;
        private System.Windows.Forms.ComboBox drp_team;
        private System.Windows.Forms.Button btn_comp_back;
        private System.Windows.Forms.ListView list_comp;
        private System.Windows.Forms.ColumnHeader clm_comp_name;
        private System.Windows.Forms.ColumnHeader clm_comp_age;
        private System.Windows.Forms.ColumnHeader clm_comp_nationality;
        private System.Windows.Forms.ColumnHeader clm_comp_role;
        private System.Windows.Forms.ColumnHeader clm_comp_team;
        private System.Windows.Forms.Button btn_comp_show;
        private System.Windows.Forms.Button btn_comp_add_display;
        private System.Windows.Forms.GroupBox grpCoach;
        private System.Windows.Forms.ComboBox drp_coach_team;
        private System.Windows.Forms.NumericUpDown num_coach_age;
        private System.Windows.Forms.Label lbl_coach_team;
        private System.Windows.Forms.Label lbl_coach_nationality;
        private System.Windows.Forms.Button btn_coach_back;
        private System.Windows.Forms.ListView list_coach;
        private System.Windows.Forms.ColumnHeader clm_coach_name;
        private System.Windows.Forms.ColumnHeader clm_coach_;
        private System.Windows.Forms.ColumnHeader clm_coach_nationality;
        private System.Windows.Forms.ColumnHeader clm_coach_team;
        private System.Windows.Forms.Button btn_coach_show;
        private System.Windows.Forms.Button btn_coach_add_display;
        private System.Windows.Forms.Button btn_coach_add_submit;
        private System.Windows.Forms.Label lbl_coach_age;
        private System.Windows.Forms.Label lbl_coach_name;
        private System.Windows.Forms.TextBox txt_coach_nationality;
        private System.Windows.Forms.TextBox txt_coach_name;
        private System.Windows.Forms.GroupBox grpSkill;
        private System.Windows.Forms.ListView list_skill_player;
        private System.Windows.Forms.ColumnHeader clm_skill_player_name;
        private System.Windows.Forms.Label lbl_skill_dropSkill;
        private System.Windows.Forms.Label lbl_skill_dropPlayer;
        private System.Windows.Forms.ComboBox drp_skill_skill;
        private System.Windows.Forms.ComboBox drp_skill_player;
        private System.Windows.Forms.Button btn_skill_comp_display;
        private System.Windows.Forms.ListView list_skill;
        private System.Windows.Forms.ColumnHeader clm_skill_name;
        private System.Windows.Forms.Button btn_skill_display;
        private System.Windows.Forms.Button btn_skill_add_display;
        private System.Windows.Forms.Button btn_skill_back;
        private System.Windows.Forms.Button btn_skill_add_submit;
        private System.Windows.Forms.Label lbl_skill_name;
        private System.Windows.Forms.TextBox txt_skill_name;
        private System.Windows.Forms.Label lbl_skill_confirm;
        private System.Windows.Forms.Button btn_add_comp_skill;
        private System.Windows.Forms.GroupBox grp_player_skill;
        private System.Windows.Forms.Label lbl_player_skill_added;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox drp_player_skill_skill;
        private System.Windows.Forms.ComboBox drp_player_skill_player;
        private System.Windows.Forms.Button btn_player_skill_back;
        private System.Windows.Forms.Button btn_player_skill_add;
        private System.Windows.Forms.Label lbl_comp_added;
        private System.Windows.Forms.Label lbl_team_added;
        private System.Windows.Forms.Label lbl_coach_added;
    }
}

