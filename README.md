# competitor-ef

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
[![DLL](https://raster.shields.io/badge/Competitor-Class-informational?style=flat-square)](https://gitlab.com/pauberExperis/competitorlib)
> Competitor Entity Framework Assignment

## Table of Contents

- [Run](#run)
- [Maintainers](#maintainers)
- [Components](#components)
- [Contributing](#contributing)
- [License](#license)

## Run
```
Visual Studio Run
```
## Components

Competitor Forms with database, using competitor class library and entity framework, see badge for class lib

## Maintainers

[Philip Aubert (@pauberexperis)](https://gitlab.com/pauberexperis)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

Experis © 2020 Noroff Accelerate AS