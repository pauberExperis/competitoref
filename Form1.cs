﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Collection;
using Microsoft.EntityFrameworkCore;

namespace CompetitorEF
{
    public partial class Form1 : Form
    {
        List<Organisation> orgs;
        BindingSource orgBind = new BindingSource();
        BindingSource skillBind = new BindingSource();
        BindingSource playerBind = new BindingSource();
        Competitor selectedCompetitor;
        Skill selectedSkill;
        List<Skill> champs;
        List<Competitor> players;

        //temp competitor fields
        public string compName = "";
        public int compAge = 0;
        public string compNationality = "";
        public string compRole = "";
        public int compOrganisationId = 0;

        //temp team fields
        public string teamName = "";
        public string teamRegion = "";

        //temp coach fields
        public string coachName = "";
        public int coachAge = 0;
        public string coachNationality = "";
        public int coachOrganisationId = 0;

        //temp skill fields
        public string skillName = "";

        public Form1()
        {
            InitializeComponent();
        }

        private void txt_name_TextChanged(object sender, EventArgs e)
        {
            compName = txt_name.Text;
        }

        private void txt_nationality_TextChanged(object sender, EventArgs e)
        {
            compNationality = txt_nationality.Text;
        }

        private void txt_role_TextChanged(object sender, EventArgs e)
        {
            compRole = txt_role.Text;
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            compAge = (int)numericUpDown1.Value;
        }

        private void btn_team_add_display_Click(object sender, EventArgs e)
        {
            txt_team_name.Visible = true;
            txt_team_region.Visible = true;
            btn_team_add_submit.Visible = true;
            lbl_team_name_display.Visible = true;
            lbl_team_region_display.Visible = true;
            list_teams.Visible = false;
            lbl_team_added.Visible = false;
        }

        private void btn_team_show_Click(object sender, EventArgs e)
        {

            using (CompetitorEFDbContext comtext = new CompetitorEFDbContext())
            {
                List<Organisation> orgs = comtext.Organisations.ToList();
                list_teams.Items.Clear();
                foreach (Organisation org in orgs)
                {
                    ListViewItem itm = new ListViewItem(org.Name);
                    itm.SubItems.Add(org.Region);
                    list_teams.Items.Add(itm);
                }
            }
            txt_team_name.Visible = false;
            txt_team_region.Visible = false;
            btn_team_add_submit.Visible = false;
            lbl_team_name_display.Visible = false;
            lbl_team_region_display.Visible = false;
            list_teams.Visible = true;
            lbl_team_added.Visible = false;
        }


        private void txt_team_name_TextChanged(object sender, EventArgs e)
        {
            teamName = txt_team_name.Text;
        }

        private void txt_team_region_TextChanged(object sender, EventArgs e)
        {
            teamRegion = txt_team_region.Text;
        }

        private void btn_team_add_submit_Click(object sender, EventArgs e)
        {
            if (teamName.Length != 0 && teamRegion.Length != 0)
            {
                using (CompetitorEFDbContext comtext = new CompetitorEFDbContext())
                {
                    comtext.Organisations.Add(new Organisation(teamName, teamRegion));
                    comtext.SaveChanges();
                } 
            lbl_team_added.Text = teamName + " has been added!";
            lbl_team_added.Visible = true;
            }
           
        }

        private void list_teams_SelectedIndexChanged_1(object sender, EventArgs e)
        {

        }

        private void btn_team_back_Click(object sender, EventArgs e)
        {
            grpMenu.Visible = true;
            grpTeam.Visible = false;
            lbl_team_region_display.Visible = false;
            lbl_team_name_display.Visible = false;
            list_teams.Visible = false;
            txt_team_name.Visible = false;
            txt_team_region.Visible = false;
            lbl_team_added.Text = "";
            lbl_team_added.Visible = false;
        }

        private void btn_menu_competitors_Click(object sender, EventArgs e)
        {
            using (CompetitorEFDbContext comtext = new CompetitorEFDbContext())
            {
                orgs = comtext.Organisations.ToList();
                foreach (Organisation org in orgs)
                {
                    ListViewItem itm = new ListViewItem(org.Name);
                    itm.SubItems.Add(org.Region);
                    list_teams.Items.Add(itm);
                }
            }
            orgBind.DataSource = orgs;
            drp_team.DataSource = orgBind;
            drp_team.DisplayMember = "Name";
            drp_team.ValueMember = "Name";
            grpComp.Visible = true;
            grpMenu.Visible = false;
        }

        private void btn_menu_teams_Click(object sender, EventArgs e)
        {
            grpTeam.Visible = true;
            grpMenu.Visible = false;
        }

        private void btn_menu_coaches_Click(object sender, EventArgs e)
        {
            using (CompetitorEFDbContext comtext = new CompetitorEFDbContext())
            {
                orgs = comtext.Organisations.ToList();
                foreach (Organisation org in orgs)
                {
                    ListViewItem itm = new ListViewItem(org.Name);
                    itm.SubItems.Add(org.Region);
                    list_teams.Items.Add(itm);
                }
            }
            orgBind.DataSource = orgs;
            drp_coach_team.DataSource = orgBind;
            drp_coach_team.DisplayMember = "Name";
            drp_coach_team.ValueMember = "Name";
            grpCoach.Visible = true;
            grpMenu.Visible = false;
        }

        private void btn_menu_skills_Click(object sender, EventArgs e)
        {
            grpSkill.Visible = true;
            grpMenu.Visible = false;

        }

        private void drp_team_SelectedIndexChanged(object sender, EventArgs e)
        {
            Organisation org = (Organisation)drp_team.SelectedItem;
            compOrganisationId = org.Id;
        }

        private void btn_comp_add_Click(object sender, EventArgs e)
        {
            if (compName.Length != 0 && compOrganisationId != 0 && compAge != 0 && compRole.Length != 0 && compNationality.Length != 0)
            {
                Competitor comp = new Competitor(compName, compAge, compNationality, compRole, compOrganisationId);
                using (CompetitorEFDbContext comtext = new CompetitorEFDbContext())
                {
                    comtext.Competitors.Add(comp);
                    comtext.SaveChanges();                   
                } 
            lbl_comp_added.Text = compName + " has been added!";
            lbl_comp_added.Visible = true;
            }
           
        }

        private void btn_comp_back_Click(object sender, EventArgs e)
        {
            grpMenu.Visible = true;
            grpComp.Visible = false;
            txt_name.Visible = false;
            txt_role.Visible = false;
            drp_team.Visible = false;
            txt_nationality.Visible = false;
            numericUpDown1.Visible = false;
            btn_comp_add.Visible = false;
            lbl_comp_name.Visible = false;
            lbl_comp_role.Visible = false;
            lbl_comp_nationality.Visible = false;
            lbl_comp_age.Visible = false;
            lbl_comp_team.Visible = false;
            lbl_team_region_display.Visible = false;
            list_comp.Visible = false;
            lbl_comp_added.Text = "";
            lbl_comp_added.Visible = false;
        }

        private void txt_nationality_TextChanged_1(object sender, EventArgs e)
        {
            compNationality = txt_nationality.Text;
        }

        private void txt_role_TextChanged_1(object sender, EventArgs e)
        {
            compRole = txt_role.Text;
        }

        private void btn_comp_show_Click(object sender, EventArgs e)
        {

            using (CompetitorEFDbContext comtext = new CompetitorEFDbContext())
            {
                List<Competitor> comps = comtext.Competitors.ToList();
                list_comp.Items.Clear();
                foreach (Competitor comp in comps)
                {
                    ListViewItem itm = new ListViewItem(comp.Name);
                    itm.SubItems.Add(comp.Age.ToString());
                    itm.SubItems.Add(comp.Nationality);
                    itm.SubItems.Add(comp.Role);
                    Organisation itmOrg = comtext.Organisations.Where(o => o.Id == comp.OrganisationId).Single();
                    itm.SubItems.Add(itmOrg.Name);
                    list_comp.Items.Add(itm);
                }
            }
            txt_name.Visible = false;
            txt_role.Visible = false;
            drp_team.Visible = false;
            txt_nationality.Visible = false;
            numericUpDown1.Visible = false;
            btn_comp_add.Visible = false;
            lbl_comp_name.Visible = false;
            lbl_comp_role.Visible = false;
            lbl_comp_nationality.Visible = false;
            lbl_comp_age.Visible = false;
            lbl_comp_team.Visible = false;
            lbl_team_region_display.Visible = false;
            list_comp.Visible = true;
            lbl_comp_added.Visible = false;
        }

        private void btn_comp_add_display_Click(object sender, EventArgs e)
        {
            txt_name.Visible = true;
            txt_role.Visible = true;
            drp_team.Visible = true;
            txt_nationality.Visible = true;
            numericUpDown1.Visible = true;
            btn_comp_add.Visible = true;
            lbl_comp_name.Visible = true;
            lbl_comp_role.Visible = true;
            lbl_comp_nationality.Visible = true;
            lbl_comp_age.Visible = true;
            lbl_comp_team.Visible = true;
            lbl_team_region_display.Visible = true;
            list_comp.Visible = false;
            lbl_comp_added.Visible = false;
        }

        private void list_comp_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        private void btn_coach_add_display_Click(object sender, EventArgs e)
        {
            txt_coach_name.Visible = true;
            txt_coach_nationality.Visible = true;
            drp_coach_team.Visible = true;            
            num_coach_age.Visible = true;
            btn_coach_add_submit.Visible = true;
            lbl_coach_name.Visible = true;
            lbl_coach_age.Visible = true;
            lbl_coach_nationality.Visible = true;
            lbl_coach_team.Visible = true;
            list_coach.Visible = false;
            lbl_coach_added.Visible = false;
        }

        private void btn_coach_show_Click(object sender, EventArgs e)
        {
            using (CompetitorEFDbContext comtext = new CompetitorEFDbContext())
            {
                List<Coach> coaches = comtext.Coaches.ToList();
                list_coach.Items.Clear();
                foreach (Coach coach in coaches)
                {
                    ListViewItem itm = new ListViewItem(coach.Name);
                    itm.SubItems.Add(coach.Age.ToString());
                    itm.SubItems.Add(coach.Nationality);
                    Organisation itmOrg = comtext.Organisations.Where(o => o.Id == coach.OrganisationId).Single();
                    itm.SubItems.Add(itmOrg.Name);
                    list_coach.Items.Add(itm);
                }
            }
            txt_coach_name.Visible = false;
            drp_coach_team.Visible = false;
            txt_coach_nationality.Visible = false;
            num_coach_age.Visible = false;
            btn_coach_add_submit.Visible = false;
            lbl_coach_name.Visible = false;
            lbl_coach_nationality.Visible = false;
            lbl_coach_age.Visible = false;
            lbl_coach_team.Visible = false;
            list_coach.Visible = true;
            lbl_coach_added.Visible = false;
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            coachName = txt_coach_name.Text;
        }

        private void num_coach_age_ValueChanged(object sender, EventArgs e)
        {
            coachAge = (int)num_coach_age.Value;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            coachNationality = txt_coach_nationality.Text;
        }

        private void drp_coach_team_SelectedIndexChanged(object sender, EventArgs e)
        {
            coachOrganisationId = drp_coach_team.SelectedIndex + 1;
        }

        private void btn_coach_add_submit_Click(object sender, EventArgs e)
        {
            if (coachName.Length != 0 && coachOrganisationId != 0 && coachAge != 0 && coachNationality.Length != 0)
            {
                Coach coach= new Coach(coachName, coachAge, coachNationality, coachOrganisationId);
                using (CompetitorEFDbContext comtext = new CompetitorEFDbContext())
                {
                    comtext.Coaches.Add(coach);
                    comtext.SaveChanges();
                }
                lbl_coach_added.Text = coachName + " has been added!";
                lbl_coach_added.Visible = true;
            }
            

        }

        private void btn_coach_back_Click(object sender, EventArgs e)
        {
            grpMenu.Visible = true;
            grpCoach.Visible = false;
            txt_coach_name.Visible = false;
            drp_coach_team.Visible = false;
            txt_coach_nationality.Visible = false;
            num_coach_age.Visible = false;
            btn_coach_add_submit.Visible = false;
            lbl_coach_name.Visible = false;
            lbl_coach_nationality.Visible = false;
            lbl_coach_age.Visible = false;
            lbl_coach_team.Visible = false;
            list_coach.Visible = false;
            lbl_coach_added.Text = "";
            lbl_coach_added.Visible = false;
        }

        private void txt_skill_name_TextChanged(object sender, EventArgs e)
        {
            skillName = txt_skill_name.Text;
        }

        private void btn_skill_add_submit_Click(object sender, EventArgs e)
        {
            if (skillName.Length != 0)
            {
                Skill skill = new Skill(skillName);
                using (CompetitorEFDbContext comtext = new CompetitorEFDbContext())
                {
                    comtext.Skills.Add(skill);
                    comtext.SaveChanges();
                    lbl_skill_confirm.Text = skillName + " has been added!";
                    lbl_skill_confirm.Visible = true;
                }

            }
        }

        private void btn_skill_back_Click(object sender, EventArgs e)
        {
            grpMenu.Visible = true;
            grpSkill.Visible = false;
            btn_skill_add_submit.Visible = false;
            lbl_skill_name.Visible = false;
            txt_skill_name.Visible = false;
            list_skill.Visible = false;
            list_skill_player.Visible = false;
            lbl_skill_dropSkill.Visible = false;
            lbl_skill_dropPlayer.Visible = false;
            drp_skill_player.Visible = false;
            drp_skill_skill.Visible = false;
            lbl_skill_confirm.Visible = false;
            lbl_skill_confirm.Text = "";
            btn_add_comp_skill.Visible = false;
        }

        private void drp_skill_player_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedCompetitor = (Competitor)drp_skill_player.SelectedItem;
            using (CompetitorEFDbContext comtext = new CompetitorEFDbContext())
            {
                list_skill_player.Clear();
                List<Skill> tempSkills = comtext.Skills.Where(s => s.CompetitorSkills.Any(u => u.CompetitorId == selectedCompetitor.Id)).ToList();
                foreach(Skill skill in tempSkills)
                {
                    ListViewItem itm = new ListViewItem(skill.Name);
                    list_skill_player.Items.Add(itm);
                    Console.WriteLine(  skill.Name);
                }
               
            }
        }

        private void drp_skill_skill_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedSkill = (Skill)drp_skill_skill.SelectedItem;
            using (CompetitorEFDbContext comtext = new CompetitorEFDbContext())
            {
                list_skill_player.Clear();
                List<Competitor> tempCompetitors = comtext.Competitors.Where(s => s.CompetitorSkills.Any(u => u.SkillId == selectedSkill.Id)).ToList();
                foreach (Competitor comp in tempCompetitors)
                {
                    list_skill_player.Items.Add(comp.Name);
                    Console.WriteLine(comp.Name);
                }

            }
        }

        private void btn_skill_comp_display_Click(object sender, EventArgs e)
        {
            using (CompetitorEFDbContext comtext = new CompetitorEFDbContext())
            {
                players = comtext.Competitors.ToList();
                champs = comtext.Skills.ToList();
                
            }
            playerBind.DataSource = players;
            drp_skill_player.DataSource = playerBind;
            drp_skill_player.DisplayMember = "Name";
            drp_skill_player.ValueMember = "Name";

            skillBind.DataSource = champs;
            drp_skill_skill.DataSource = skillBind;
            drp_skill_skill.DisplayMember = "Name";
            drp_skill_skill.DisplayMember = "Name";

            drp_player_skill_player.DataSource = playerBind;
            drp_player_skill_player.DisplayMember = "Name";
            drp_player_skill_player.ValueMember = "Name";

            drp_player_skill_skill.DataSource = skillBind;
            drp_player_skill_skill.DisplayMember = "Name";
            drp_player_skill_skill.ValueMember = "Name";

            btn_skill_add_submit.Visible = false;
            lbl_skill_name.Visible = false;
            txt_skill_name.Visible = false;
            list_skill.Visible = false;
            list_skill_player.Visible = true;
            lbl_skill_dropSkill.Visible = false;
            lbl_skill_dropPlayer.Visible = false;
            drp_skill_player.Visible = true;
            drp_skill_skill.Visible = true;
            lbl_skill_confirm.Visible = false;
            btn_add_comp_skill.Visible = true ;

        }

        private void btn_skill_display_Click(object sender, EventArgs e)
        {
            using (CompetitorEFDbContext comtext = new CompetitorEFDbContext())
            {
                List<Skill> skills = comtext.Skills.ToList();
                list_skill.Items.Clear();
                foreach (Skill skill in skills)
                {
                    ListViewItem itm = new ListViewItem(skill.Name);
                    list_skill.Items.Add(itm);
                }
            }

            btn_skill_add_submit.Visible = false;
            lbl_skill_name.Visible = false;
            txt_skill_name.Visible = false;
            list_skill.Visible = true;
            list_skill_player.Visible = false;
            lbl_skill_dropSkill.Visible = false;
            lbl_skill_dropPlayer.Visible = false;
            drp_skill_player.Visible = false;
            drp_skill_skill.Visible = false;
            lbl_skill_confirm.Visible = false;
            btn_add_comp_skill.Visible = false;
        }

        private void btn_skill_add_display_Click(object sender, EventArgs e)
        {
            btn_skill_add_submit.Visible = true;
            lbl_skill_name.Visible = true;
            txt_skill_name.Visible = true;
            list_skill.Visible = false;
            list_skill_player.Visible = false;
            lbl_skill_dropSkill.Visible = false;
            lbl_skill_dropPlayer.Visible = false;
            drp_skill_player.Visible = false;
            drp_skill_skill.Visible = false;
            lbl_skill_confirm.Visible = true;
            btn_add_comp_skill.Visible = false;
        }

        private void btn_add_comp_skill_Click(object sender, EventArgs e)
        {
            grp_player_skill.Visible = true;
            btn_add_comp_skill.Visible = false;
            list_skill_player.Visible = false;
            drp_skill_player.Visible = false;
            drp_skill_skill.Visible = false;
        }

        private void btn_player_skill_add_Click(object sender, EventArgs e)
        {
            using (CompetitorEFDbContext comtext = new CompetitorEFDbContext())
            {
                comtext.CompetitorSkills.Add(new CompetitorSkill() { CompetitorId = selectedCompetitor.Id, SkillId = selectedSkill.Id });
                comtext.SaveChanges();
                lbl_player_skill_added.Text = selectedCompetitor.Name + " can now play " + selectedSkill.Name + "!";
               
            }
        }

        private void drp_player_skill_player_SelectedIndexChanged(object sender, EventArgs e)
        {
                selectedCompetitor = (Competitor)drp_player_skill_player.SelectedItem;
        }

        private void drp_player_skill_skill_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedSkill = (Skill)drp_player_skill_skill.SelectedItem;
        }

        private void btn_player_skill_back_Click(object sender, EventArgs e)
        {
            grp_player_skill.Visible = false;
            btn_add_comp_skill.Visible = false;
            list_skill_player.Visible = false;
            drp_skill_player.Visible = false;
            drp_skill_skill.Visible = false;
        }
    }
}
