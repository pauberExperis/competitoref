﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Collection;
using Microsoft.EntityFrameworkCore;

namespace CompetitorEF
{
    class CompetitorEFDbContext:DbContext
    {
        public DbSet<Competitor> Competitors { get; set; }
        public DbSet<Coach> Coaches { get; set; }
        public DbSet<Organisation> Organisations { get; set; }
        public DbSet<Skill> Skills { get; set; }
        public DbSet<CompetitorSkill> CompetitorSkills { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=PC7364\SQLEXPRESS;Database=Comformdb;Trusted_Connection=true;MultipleActiveResultSets=true;");
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CompetitorSkill>().HasKey(cs => new { cs.CompetitorId, cs.SkillId });
        }


    }
}
